#include "Spawner.h"
#include "Characters/Food.h"
#include "Characters/SpeedFood.h"
#include "Kismet/KismetMathLibrary.h"

ASpawner::ASpawner()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	CreateFood();
}

void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FTransform ASpawner::GetLocationForNewObject() const
{
	const float FoodLocationY = FMath::RandRange(MinY, MaxY);
	const float FoodLocationX = FMath::RandRange(MinX, MaxX);

	const FVector Location(FoodLocationX, FoodLocationY, 0);
	const FTransform STransform(Location);
	return STransform;
}

void ASpawner::CreateFood()
{
	const FTransform FoodTransform = GetLocationForNewObject();
	AFood* NewFood;
	
	if (UKismetMathLibrary::RandomBoolWithWeight(0.5f))
	{
		NewFood = GetWorld()->SpawnActor<ASpeedFood>(SpeedFoodClass, FoodTransform);	
	}
	else
	{
		NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);	
	}
	
	if (UKismetMathLibrary::RandomBoolWithWeight(ChangeWeightToCreateBomb))
	{
		const FTransform DamageTransform = GetLocationForNewObject();
		ADamage* Damage = GetWorld()->SpawnActor<ADamage>(DamageClass, DamageTransform);
		NewFood->SetDamage(Damage);
	}
}

