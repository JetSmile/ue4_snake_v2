#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Damage.generated.h"

UCLASS()
class SNAKEGAME_API ADamage : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ADamage();
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
private:
	UPROPERTY()
	float CurrentLife;
};
