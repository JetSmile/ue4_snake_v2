#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFood::SetDamage(ADamage* Damage)
{
	DamageFood = Damage;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Objects;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), SpawnerClass, Objects);
	if (Objects.Num() == 1)
	{
		Spawner = Cast<ASpawner>(Objects[0]);
	}
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (bIsHead)
		{
			Snake->AddSnakeElement(Snake->SnakeElementClass);
		}

		if (DamageFood && IsValid(DamageFood))
		{
			DamageFood->Destroy();
		}
		
		Spawner->CreateFood();
		//this->
		Destroy();
	}
}

