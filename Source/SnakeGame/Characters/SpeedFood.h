// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "SpeedFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASpeedFood : public AFood
{
	GENERATED_BODY()

	ASpeedFood();	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
