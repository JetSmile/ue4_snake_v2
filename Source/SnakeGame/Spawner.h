#pragma once

#include "CoreMinimal.h"
#include "Characters/Damage.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class AFood;

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()

public:
	ASpawner();

	UPROPERTY(EditDefaultsOnly, Category="Location")
	float MinX = -490.f;

	UPROPERTY(EditDefaultsOnly, Category="Location")
	float MinY = -490.f;

	UPROPERTY(EditDefaultsOnly, Category="Location")
	float MaxX = 490.f;

	UPROPERTY(EditDefaultsOnly, Category="Location")
	float MaxY = 490.f;

	UPROPERTY(EditDefaultsOnly)
	float ChangeWeightToCreateBomb = 0.3f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> SpeedFoodClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADamage> DamageClass;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void CreateFood();

private:
	FTransform GetLocationForNewObject() const;
};
